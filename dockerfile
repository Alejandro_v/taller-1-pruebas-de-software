#
# Build stage
#
FROM maven:3.6.0-jdk-11-slim AS build
COPY src /home/app/src
COPY pom.xml /home/app
RUN mvn -f /home/app/pom.xml clean package

RUN mvn -f /home/app/pom.xml clean verify sonar:sonar \
  -Dsonar.projectKey=Alma \
  -Dsonar.host.url=http://172.17.0.2:9000 \
  -Dsonar.login=sqp_65ec83657dfd12b2ce4ce81ad40bcfc085c537d9
#
# Package stage
#
FROM openjdk:11-jre-slim
COPY --from=build /home/app/target/almamater-0.0.1-SNAPSHOT.jar /usr/local/lib/alma.jar
EXPOSE 8080
ENTRYPOINT ["java","-jar","/usr/local/lib/alma.jar"]
